/* ************************************************************************************************
* Skeleton implementation for executing measurements with different sensors on the BLE 33 Sense. 
*   For the most part, the statemachine is already pretty much there, just fill in the measurement
*   and display steps as needed. 
*   
*   The SensorData class needs a few public fields to store the data as it is measured. 
*   
************************************************************************************************ */

/* ************************************************
* Includes:
************************************************ */
#include <ArduinoBLE.h>
#include <Arduino_HTS221.h>


/* ************************************************
* Enums and class declarations:
************************************************ */

/**
* General definition of states for the main StateMachine. 
*/
enum MachineState 
{
  StateInitSensor,
  StateIdle,
  StateConnected, 
  StateReadSensor,
  StateSendData
};


/**
* Class to maintain all relevant states of the application. Current state, behaviour manipulation and functions to update / read values. 
*/
class ApplicationState
{
  public:
    ApplicationState();
    void advance_machine_state();
    void mark_measurement_time();
    bool evaluate_interval_passed(unsigned long interval);
    MachineState get_current_machinestate();
    void set_ble_connected(bool is_connected);
    
  private:
    unsigned long last_measurement_msec;    
    
    MachineState curr_machine_state;
    bool ble_connected;
};


/**
* Data model for storing, maintaining and recovery of last measurements. 
*/
class SensorData
{
  public:
    SensorData();

    // Measurement fields go here. 
    float temperature;
};


/* ************************************************
* Class implementations:
************************************************ */

/**
* ApplicationState constructor. Upon instantiation, sets initial machine state and last_measurement_sec. 
*/
ApplicationState::ApplicationState() 
{
  this->curr_machine_state    = StateInitSensor;
  this->last_measurement_msec = 0;
  this->ble_connected         = false;
}


/**
* advance_machine_state: Updates machinestate, depending on current machinestate. In this case it's all pretty linear. 
*/
void ApplicationState::advance_machine_state()
{
  switch(this->curr_machine_state)
  {
    case StateInitSensor:         this->curr_machine_state = StateIdle;       break;  
    case StateIdle:               this->curr_machine_state = StateConnected;  break; 
    
    case StateConnected:          
      // If we're connected: go to read sensor. Then break; 
      if(this->ble_connected)
      {
        this->curr_machine_state = StateReadSensor;

        break; // case StateConnected
      }
      
      // go to idle.
      this->curr_machine_state = StateIdle;               
      
      break;  // case StateConnected
      
    case StateReadSensor: this->curr_machine_state = StateSendData;   break; 
    case StateSendData:   this->curr_machine_state = StateConnected;  break;
  }
}


/**
* mark_measurement_time: Set last measurement time of application instance to current time in milliseconds. The interval will then use this
*   to determine the next measurement. 
*/
void ApplicationState::mark_measurement_time()
{
  this->last_measurement_msec = millis();
}


/**
* evaluate_interval_passed: Returns true if more than 'interval' milliseconds have past since last time a measurement was initiated. 
* 
*   Input: 
*     unsigned long interval: Interval in milliseconds to compare to current time and last measurement. 
*/
bool ApplicationState::evaluate_interval_passed(unsigned long interval)
{
  unsigned long curr_time = millis();

  // If current time > interval (because of next calculation) and curr_time - interval still supersedes last measurement time, 
  //   it's time again.
  return curr_time > interval &&
    (curr_time - interval) >= this->last_measurement_msec;
}


/**
* get_current_machinestate: Return current machinestate from private field. 
*/
MachineState ApplicationState::get_current_machinestate()
{
  return this->curr_machine_state;
}


void ApplicationState::set_ble_connected(bool is_connected)
{
  this->ble_connected = is_connected;  
}


/**
* Constructor. Initialize data object's fields to 0 / startpoint.
*/
SensorData::SensorData()
{
  // Set measurement values initially to 0. 
  this->temperature = 0;
}


/* ************************************************
* Global variables:
************************************************ */
ApplicationState  app_state;
SensorData        sensor1;


const char* nameOfPeripheral  = "DataIO";
const char* uuidOfService     = "00001101-0000-1000-8000-00805f9b34fb";
const char* uuidOfRxChar      = "00001142-0000-1000-8000-00805f9b34fb";
const char* uuidOfTxChar      = "00001143-0000-1000-8000-00805f9b34fb";

BLEService dataIOService(uuidOfService);
BLEDevice  ble_device;

const int WRITE_BUFFER_SIZE     = 256;
bool WRITE_BUFFER_FIXED_LENGTH  = false;

// RX / TX Characteristics
BLECharacteristic       rxChar(uuidOfRxChar, BLEWriteWithoutResponse | BLEWrite, WRITE_BUFFER_SIZE, WRITE_BUFFER_FIXED_LENGTH);
BLEFloatCharacteristic  txChar(uuidOfTxChar, BLERead | BLENotify | BLEBroadcast);


/* ************************************************
* Main application:
************************************************ */


/**
* Setup core of Arduino. 
*/
void setup() 
{
  Serial.begin(9600);
  while (!Serial);

}


/**
* Write to terminal a label, value and unit to display sensor readings. 
*/
void print_data(char *label, char *unit, float value)
{
  Serial.print(label);
  Serial.print(" = ");
  Serial.print(value);
  Serial.println(unit);  
}


void print_BLE_init_data()
{
  // Print out full UUID and MAC address.
  Serial.println("Peripheral advertising info: ");
  Serial.print("Name: ");
  Serial.println(nameOfPeripheral);
  Serial.print("MAC: ");
  Serial.println(BLE.address());
  Serial.print("Service UUID: ");
  Serial.println(dataIOService.uuid());
  Serial.print("rxCharacteristic UUID: ");
  Serial.println(uuidOfRxChar);
  Serial.print("txCharacteristics UUID: ");
  Serial.println(uuidOfTxChar);
}


/*
 *  BLUETOOTH
 */
void startBLE() {
  if (!BLE.begin())
  {
    Serial.println("starting BLE failed!");
    while (1);
  }
}


void onRxCharValueUpdate(BLEDevice central, BLECharacteristic characteristic) 
{
  // central wrote new value to characteristic, update LED
  Serial.print("Characteristic event, read: ");
  byte test[256];
  int dataLength = rxChar.readValue(test, 256);

  for(int i = 0; i < dataLength; i++) 
  {
    Serial.print((char)test[i]);
  }
  
  Serial.println();
}


void onBLEConnected(BLEDevice central) 
{
  Serial.print("Connected event, central: ");
  Serial.println(central.address());
}


void onBLEDisconnected(BLEDevice central) 
{
  Serial.print("Disconnected event, central: ");
  Serial.println(central.address());
}


/**
* statemachine: Main statemachine that runs the application. No state is blocking. Rather, the idle function (probably running most of the 
*   time) should evaluate the intervaltime for measurement or other tasks. 
*/
void statemachine()
{
  ble_device = BLE.central();
  
  switch(app_state.get_current_machinestate())
  {
    case StateInitSensor: 
      // Initialize Sensor. 
      if (!HTS.begin()) 
      {
        Serial.println("Failed to initialize humidity temperature sensor!");
        while (1);
      }
      
      Serial.println("Temperature Sensor initialized.");

      // Start BLE.
      startBLE();
    
      // Create BLE service and characteristics.
      BLE.setLocalName(nameOfPeripheral);
      BLE.setAdvertisedService(dataIOService);
      dataIOService.addCharacteristic(rxChar);
      dataIOService.addCharacteristic(txChar);
      BLE.addService(dataIOService);
    
      // Bluetooth LE connection handlers.
      BLE.setEventHandler(BLEConnected,     onBLEConnected);
      BLE.setEventHandler(BLEDisconnected,  onBLEDisconnected);
      
      // Event driven reads.
      rxChar.setEventHandler(BLEWritten, onRxCharValueUpdate);

      // Let's tell devices about us.
      BLE.advertise();     
      
      print_BLE_init_data();
      
      // next step in application.
      app_state.advance_machine_state();
      
      break;  // case StateInitSensor
      
    case StateIdle: 
      // Check connection status. If connected, go to StateConnected. 
      if(ble_device && ble_device.connected())
      {
        Serial.println("Marking connected.");
        app_state.set_ble_connected(true);
        app_state.advance_machine_state();
      }
      
      break; // case StateIdle

    case StateConnected: 
      // If no longer connected, drop back to idle. then break;
      if(!ble_device || !ble_device.connected())
      {
        Serial.println("Marking disconnected.");
        app_state.set_ble_connected(false);
        app_state.advance_machine_state();
      }
      
      // If interval passed: Read temperarure sensor and return value. 
      if(app_state.evaluate_interval_passed(1000UL))
      {
        app_state.advance_machine_state();
      }
      
      break; // case StateConnected;
      
    case StateReadSensor: 
      // Execute actual read of sensor. 
      app_state.mark_measurement_time();
      
      // Execute actual measurements and store them in the datamodel. 
      sensor1.temperature = HTS.readTemperature();
      
      // Advance statemachine. 
      app_state.advance_machine_state();
      
      break; // case StateReadSensor
      
    case StateSendData: 
      // Send measurement to client.
      txChar.writeValue(sensor1.temperature);      
      
      // Advance statemachine. 
      app_state.advance_machine_state();
      
      break;  // case StateDisplayMeasurement
  }
}


/***
* loop: Executed indefinitely, just execute the statemachine. 
*/
void loop()
{
  statemachine();  
}
