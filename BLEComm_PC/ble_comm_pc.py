"""
Companion python application for the roadtest in which I setup the Arduino Nano 33 BLE Sense as a
temperature bluetooth device and connect to it with this script. 

While DEFINITELY not a carbon copy, I took a lot of work and inspiration from: 
https://ladvien.com/python-serial-terminal-with-arduino-and-bleak/

My work is just a lot more barebone and probably a little less robust. I just didn't need all the
peripherals around it. 

To get bleak: Just 'pip install bleak'.
"""
from bleak import BleakClient, discover
from typing import Any

import asyncio
import struct

# ASyncIO loop object that will be passed along: 
loop = None
ble_device = None
is_connected = False

# The name the device advertises with, along with the UUID's. 
target_device_name = 'DataIO'
read_characteristic = "00001143-0000-1000-8000-00805f9b34fb"
write_characteristic = "00001142-0000-1000-8000-00805f9b34fb"


async def select_device(): 
	"""
	select_device: Go through the list of devices in range and evaluate each to connect. If a 
	'target_device_name' is found in the list, connect to it. 
	"""
	global ble_device
	global loop

	print('Scanning for devices.... ')
	devices = await discover()
	print('Devices visible: ')

	index = 0
	selected = -1

	for device in devices: 
		print('device: %s - address: %s' % (device.name, device.address))
		if device.name == target_device_name: 
			selected = index

		index += 1

	print('device found: index: %d' % selected)
	if selected > -1: 
		print('creating new BleakClient, with address: %s.... ' % devices[selected].address, end='')
		ble_device = BleakClient(devices[selected].address, loop=loop)
		print('Done')



def notification_handler(sender: str, data: Any):
	"""
	notification_handler: Triggered on incoming data. This function is set as callback when data
	  is received from the device. Incoming data could be anything and it's up to the handler to 
	  handle it correctly. In this  specific case we're expecting floats depicting a temperature. 

	:param sender: reference to source. 
	:param data: data received from source.
	"""

	print('message received from sender: %s: ' % sender)
	float_data = struct.unpack('f', data)
	print('temperature: %f celcius.' % float_data)


def on_disconnect(self, client: BleakClient, future: asyncio.Future):
	"""
	on_disconnect: callback function to recognice and handle the fact the server disconnected.
	"""
	global is_connected

	# Put code here to handle what happens on disconnet.
	print(f"Disconnected from {ble_device.name}!")
	is_connected = False



async def connect(): 
	"""
	connect: Actually connect to a given device. If device was not found in the select_device 
	  function, this function returns after an error message. 
	"""
	global is_connected

	if not ble_device: 
		print('connect: No device was selected to connect to.')
		return 

	print('connecting...')
	await ble_device.connect()
	is_connected = await ble_device.is_connected()

	if not is_connected: 
		print('Failed to connect!')
		return

	print('Connected.')
	ble_device.set_disconnected_callback(on_disconnect)
	await ble_device.start_notify(read_characteristic, notification_handler,)

	while True: 
		await asyncio.sleep(3.0, loop=loop)    	



async def run_application(): 
	"""
	run_application: Async function called by asyncio to start the actual application. This function 
	  kicks off select_device and connect (which in turn are async functions) sequentially. 
	"""
	await select_device()
	await connect()


def main():
	"""
	point of entry. Fetch the event loop from asyncio (we'll be needing it later) and kick off 
	  run_application. Then run indefinitely. 
	"""
	global loop
	
	loop = asyncio.get_event_loop()
	
	asyncio.ensure_future(run_application())
	loop.run_forever()


# start main
if __name__ == '__main__' : 
	main()
 