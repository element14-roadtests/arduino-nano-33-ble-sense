#include "led_bar.h"
// #include "SR_74HC595.h"

// SR_74HC595 shift_reg(D11, D10, D12);
LedBar led_bar1(D11, D10, D12);
byte data[] = {0, 0};

void setup() 
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  while (!Serial);
}


void loop() 
{
  // put your main code here, to run repeatedly:

  
  for(int i = 0; i < 256; i++)
  {
    data[0] = i;
    // shift_reg.display_data(i);
    
    led_bar1.show_data(data);
    delay(500);
    Serial.print("data[0]: ");
    Serial.println(data[0]);
    Serial.print("data[1]: ");
    Serial.println(data[1]);
  }

  data[1] = data[1] == 255 ? 0 : data[1] + 1; 
}
