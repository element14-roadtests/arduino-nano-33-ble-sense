/* ************************************************************************************************
* Skeleton implementation for executing measurements with different sensors on the BLE 33 Sense. 
*   For the most part, the statemachine is already pretty much there, just fill in the measurement
*   and display steps as needed. 
*   
*   The SensorData class needs a few public fields to store the data as it is measured. 
*   
************************************************************************************************ */

/* ************************************************
* Includes:
************************************************ */
#include <PDM.h>


/* ************************************************
* Enums and class declarations:
************************************************ */

/**
* General definition of states for the main StateMachine. 
*/
enum MachineState 
{
  StateInitSensor,
  StateIdle,
  StateReadSensor,
  StateDisplayMeasurement
};


/**
* Class to maintain all relevant states of the application. Current state, behaviour manipulation and functions to update / read values. 
*/
class ApplicationState
{
  public:
    ApplicationState();
    void advance_machine_state();
    void mark_measurement_time();
    bool evaluate_interval_passed(unsigned long interval);
    MachineState get_current_machinestate();
    
  private:
    MachineState curr_machine_state;
    unsigned long last_measurement_msec;    
};


/**
* Data model for storing, maintaining and recovery of last measurements. 
*/
class SensorData
{
  public:
    SensorData();

    // Measurement fields go here. 
    volatile int samples_read;
    short sample_buffer[256];
    bool has_data;
};


/* ************************************************
* Class implementations:
************************************************ */

/**
* ApplicationState constructor. Upon instantiation, sets initial machine state and last_measurement_sec. 
*/
ApplicationState::ApplicationState() 
{
  this->curr_machine_state    = StateInitSensor;
  this->last_measurement_msec = 0;
}


/**
* advance_machine_state: Updates machinestate, depending on current machinestate. In this case it's all pretty linear. 
*/
void ApplicationState::advance_machine_state()
{
  switch(this->curr_machine_state)
  {
    case StateInitSensor:         this->curr_machine_state = StateIdle;               break;  
    case StateIdle:               this->curr_machine_state = StateDisplayMeasurement; break; 
    // case StateReadSensor:         this->curr_machine_state = StateDisplayMeasurement; break; 
    case StateDisplayMeasurement: this->curr_machine_state = StateIdle;               break;
    default:                      /* shouldn't happen...*/                            break;
  }
}


/**
* mark_measurement_time: Set last measurement time of application instance to current time in milliseconds. The interval will then use this
*   to determine the next measurement. 
*/
void ApplicationState::mark_measurement_time()
{
  this->last_measurement_msec = millis();
}


/**
* evaluate_interval_passed: Returns true if more than 'interval' milliseconds have past since last time a measurement was initiated. 
* 
*   Input: 
*     unsigned long interval: Interval in milliseconds to compare to current time and last measurement. 
*/
bool ApplicationState::evaluate_interval_passed(unsigned long interval)
{
  unsigned long curr_time = millis();

  // If current time > interval (because of next calculation) and curr_time - interval still supersedes last measurement time, 
  //   it's time again.
  return curr_time > interval &&
    (curr_time - interval) >= this->last_measurement_msec;
}


/**
* get_current_machinestate: Return current machinestate from private field. 
*/
MachineState ApplicationState::get_current_machinestate()
{
  return this->curr_machine_state;
}


/**
* Constructor. Initialize data object's fields to 0 / startpoint.
*/
SensorData::SensorData()
{
  // Set measurement values initially to 0. 
  this->samples_read  = 0;
  this->has_data      = false;
}


/* ************************************************
* Global variables:
************************************************ */
ApplicationState  app_state;
SensorData        sensor1;


/**
* Setup core of Arduino. 
*/
void setup() 
{
  Serial.begin(9600);
  while (!Serial);

}


/**
* Write to terminal a label, value and unit to display sensor readings. 
*/
void print_data(char *label, char *unit, float value)
{
  Serial.print(label);
  Serial.print(" = ");
  Serial.print(value);
  Serial.println(unit);  
}


/**
* onPDMdata: Callback function that processes PDM data and stores it in sensor1 data object. 
*   This function is passed as callback handle before starting the PDM sensor. 
*/
void onPDMdata() 
{
  // query the number of bytes available
  int bytesAvailable = PDM.available();

  // read into the sample buffer
  int bytesRead = PDM.read(sensor1.sample_buffer, bytesAvailable);

  // 16-bit, 2 bytes per sample
  sensor1.samples_read  = bytesRead / 2;
  sensor1.has_data      = true;
}


/**
* print_samples: First attempt at dumping data that was gathered on-event. For each sample in the range given in 'samples_read', write 
*   to Serial. Break at appropriate line length. 
* 
*   Input: 
*     SensorData *pData: Sensor for which to write to serial. 
*/
void print_samples(SensorData *pData)
{
  for(int i = 0; i < pData->samples_read; i++)
  {
    if(i > 0 && (i % 16) == 0)
    {
      Serial.println(pData->sample_buffer[i]);
    }
    else
    {
      Serial.print(pData->sample_buffer[i]);
    }
  }
}


/**
* print_samples_seq: More plottable version of output. Just single line of output. 
* 
*   Input: 
*     SensorData *pData: Reference to sensor to plot data from. 
*/
void print_samples_seq(SensorData *pData)
{
  for(int i = 0; i < pData->samples_read; i++)
  {
    Serial.println(pData->sample_buffer[i]);
  }  
}
  
/**
* statemachine: Main statemachine that runs the application. No state is blocking. Rather, the idle function (probably running most of the 
*   time) should evaluate the intervaltime for measurement or other tasks. 
*/
void statemachine()
{
  switch( app_state.get_current_machinestate())
  {
    case StateInitSensor: 
      // Initialize Sensor. 
      // Set callback
      PDM.onReceive(onPDMdata);

      // Sensor begin.
      if (!PDM.begin(1, 16000)) 
      {
        Serial.println("Failed to start PDM!");
        while (1);
      }
      
      Serial.println("Sensor initialized.");
      app_state.advance_machine_state();
      
      break;  // case StateInitSensor
      
    case StateIdle: 
      // Idle until there's actually data. Then jump to StateDisplayMeasurement.
      if(sensor1.has_data)
      {
        app_state.advance_machine_state();
      }
      
      break; // case StateIdle
      
    case StateDisplayMeasurement: 
      // Display last measurement.
      
      // print_samples(&sensor1);
      // The sequential Serial output is way better plottable. 
      print_samples_seq(&sensor1);
      
      // Advance statemachine. 
      app_state.advance_machine_state();
      
      break;  // case StateDisplayMeasurement
  }
}


/***
* loop: Executed indefinitely, just execute the statemachine. 
*/
void loop()
{
  statemachine();  
}
