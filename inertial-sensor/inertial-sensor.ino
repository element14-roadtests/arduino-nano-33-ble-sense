/* ************************************************************************************************
* Skeleton implementation for executing measurements with different sensors on the BLE 33 Sense. 
*   For the most part, the statemachine is already pretty much there, just fill in the measurement
*   and display steps as needed. 
*   
*   The SensorData class needs a few public fields to store the data as it is measured. 
*   
************************************************************************************************ */

/* ************************************************
* Includes:
************************************************ */
#include <Arduino_LSM9DS1.h>


/* ************************************************
* Enums and class declarations:
************************************************ */

/**
* General definition of states for the main StateMachine. 
*/
enum MachineState 
{
  StateInitSensor,
  StateIdle,
  StateReadSensor,
  StateDisplayMeasurement
};


/**
* Class to maintain all relevant states of the application. Current state, behaviour manipulation and functions to update / read values. 
*/
class ApplicationState
{
  public:
    ApplicationState();
    void advance_machine_state();
    void mark_measurement_time();
    bool evaluate_interval_passed(unsigned long interval);
    MachineState get_current_machinestate();
    
  private:
    MachineState curr_machine_state;
    unsigned long last_measurement_msec;    
};


/**
* Data model for storing, maintaining and recovery of last measurements. 
*/
class SensorData
{
  public:
    SensorData();

    // Measurement fields go here. 
    float x;    
    float y;
    float z;
};


/* ************************************************
* Class implementations:
************************************************ */

/**
* ApplicationState constructor. Upon instantiation, sets initial machine state and last_measurement_sec. 
*/
ApplicationState::ApplicationState() 
{
  this->curr_machine_state    = StateInitSensor;
  this->last_measurement_msec = 0;
}


/**
* advance_machine_state: Updates machinestate, depending on current machinestate. In this case it's all pretty linear. 
*/
void ApplicationState::advance_machine_state()
{
  switch(this->curr_machine_state)
  {
    case StateInitSensor:         this->curr_machine_state = StateIdle;               break;  
    case StateIdle:               this->curr_machine_state = StateReadSensor;         break; 
    case StateReadSensor:         this->curr_machine_state = StateDisplayMeasurement; break; 
    case StateDisplayMeasurement: this->curr_machine_state = StateIdle;               break;
  }
}


/**
* mark_measurement_time: Set last measurement time of application instance to current time in milliseconds. The interval will then use this
*   to determine the next measurement. 
*/
void ApplicationState::mark_measurement_time()
{
  this->last_measurement_msec = millis();
}


/**
* evaluate_interval_passed: Returns true if more than 'interval' milliseconds have past since last time a measurement was initiated. 
* 
*   Input: 
*     unsigned long interval: Interval in milliseconds to compare to current time and last measurement. 
*/
bool ApplicationState::evaluate_interval_passed(unsigned long interval)
{
  unsigned long curr_time = millis();

  // If current time > interval (because of next calculation) and curr_time - interval still supersedes last measurement time, 
  //   it's time again.
  return curr_time > interval &&
    (curr_time - interval) >= this->last_measurement_msec;
}


MachineState ApplicationState::get_current_machinestate()
{
  return this->curr_machine_state;
}


SensorData::SensorData()
{
  // Set measurement values initially to 0. 
}

/* ************************************************
* Global variables:
************************************************ */
ApplicationState  app_state;
SensorData        sensor1;


/**
* Setup core of Arduino. 
*/
void setup() 
{
  Serial.begin(9600);
  while (!Serial);

}


/**
* Write to terminal a label, value and unit to display sensor readings. 
*/
void print_data(char *label, char *unit, float value)
{
  Serial.print(label);
  Serial.print(" = ");
  Serial.print(value);
  Serial.println(unit);  
}


/**
* statemachine: Main statemachine that runs the application. No state is blocking. Rather, the idle function (probably running most of the 
*   time) should evaluate the intervaltime for measurement or other tasks. 
*/
void statemachine()
{
  switch( app_state.get_current_machinestate())
  {
    case StateInitSensor: 
      // Initialize Sensor. 
      if (!IMU.begin())
      {
        Serial.println("Failed to initialize IMU!");
        while (1);
      }

      Serial.println("Sensor initialized.");
      print_data("Gyroscope sample rate = ", "Hz", IMU.gyroscopeSampleRate());
      app_state.advance_machine_state();
      
      break;  // case StateInitSensor
      
    case StateIdle: 
      // Wait for next interval. Every 2 seconds we execute a new measurement. 
      if(app_state.evaluate_interval_passed(2000UL))
      {
        app_state.advance_machine_state();
      }
      
      break; // case StateIdle
      
    case StateReadSensor: 
      // Execute actual read of sensor. 
      app_state.mark_measurement_time();

      // Execute actual measurements and store them in the datamodel. 
      IMU.readMagneticField(sensor1.x, sensor1.y, sensor1.z);

      // Advance statemachine. 
      app_state.advance_machine_state();
      
      break; // case StateReadSensor
      
    case StateDisplayMeasurement: 
      // Display last measurement.
      print_data("x-axis = ", "", sensor1.x);
      print_data("y-axis = ", "", sensor1.y);
      print_data("z-axis = ", "", sensor1.z);
      
      // Advance statemachine. 
      app_state.advance_machine_state();
      
      break;  // case StateDisplayMeasurement
  }
}


/***
* loop: Executed indefinitely, just execute the statemachine. 
*/
void loop()
{
  statemachine();  
}
