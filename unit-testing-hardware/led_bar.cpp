/* **************************************************
* Implementation of the led-bar driver. Basically a controller 
*  for the series of shift-registers and an abstraction to control them. 
************************************************** */

#include "led_bar.h"


/**
* LedBar: Constructor. Initialize the array of shiftregisters. 
* 
*   Input: 
*     int clock_pin: Clock pin of the 595. 
*     int data_pin: Data pin of the 595. 
*     int latch_pin: Latch pin of the 595. 
*/
LedBar::LedBar(int clock_pin, int data_pin, int latch_pin)
{
  this->bars[0] = new SR_74HC595(clock_pin, data_pin, latch_pin);
  this->bars[1] = new SR_74HC595(clock_pin, data_pin, latch_pin);
}


/**
* all_off: Switch off the entire bar. 
*/
void LedBar::all_off()
{
  byte data[] = {0, 0};
  this->show_data(data);
}


/**
* show_data: Show given bytes of data. This function assumes that the data pointer references an array of 'REGISTER_COUNT' elements
* 
*   Input: 
*     byte *data: Pointer to array of bytes to show. 
*/
void LedBar::show_data(byte *data)
{
  Serial.print("Showing data: ");
  Serial.print(data[0], BIN);
  Serial.print(" ");
  Serial.println(data[1], BIN);
  
  for (int i = 0; i < 2; i++)
  {
    this->bars[i]->display_data(data[i], i == 1);
  }
}
