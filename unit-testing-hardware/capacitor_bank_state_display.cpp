/* *****************************************************
* Software driver for the actual display. 
* 
*  Buffers the states to show, offers interfaces to update 
*  these states and causes the underlying parts to update. 
* 
***************************************************** */

#include "capacitor_bank_state_display.h"


/**
* CapacitorBankStateDisplay: Constructor for the display. Create a led_bar instance and initialize local states. 
* 
*   Input: 
*       int clock_pin: Clock pin for the display
*       int data_pin: Data pin for the display
*       int latch_pin: Latch pin for the display
*       int capacitor_bank_count: Channels in the capacitor bank. 
*/
CapacitorBankStateDisplay::CapacitorBankStateDisplay(int clock_pin, int data_pin, int latch_pin, int capacitor_bank_count)
{
	this->pLed_bar 				= new LedBar(clock_pin, data_pin, latch_pin);	
	this->capacitor_bank_count 	= capacitor_bank_count;
	this->capacitor_states[0] 	= CapacitorStateLow;
	this->capacitor_states[1] 	= CapacitorStateLow;
	this->capacitor_states[2] 	= CapacitorStateLow;
	this->capacitor_states[3] 	= CapacitorStateLow;
}


/**
* display_new_state: Write all states to an array of sorrect size and then update the output. 
*/
void CapacitorBankStateDisplay::display_new_state()
{
	byte data[REGISTER_COUNT];
	
	/*
	for(int i = 0; i < 8 * REGISTER_COUNT; i++)
	{
		data = 0;
	}
	*/
	
	// Construct a new data array with all channel states. 
	for(int channel = 0; channel < 4; channel++)
	{
		int new_index 	= channel * 3 + this->capacitor_states[channel];
		int data_index 	= new_index > 7 ? 1 : 0;
		
		new_index  	   -= new_index > 7 ? 8: 0;
		data[new_index] = 1;
	}
	
    // Write new data to led-bar
	this->pLed_bar->show_data(data);
}


/**
* update_state: Update one channel with a given new state and update the display output. 
* 
* (int channel, CapacitorState state)
*/
void CapacitorBankStateDisplay::update_state(int channel, CapacitorState state)
{
	this->capacitor_states[channel] = state;
    this->display_new_state();
}
