#include <Arduino.h>
#include "SR_74HC595.h"

#define REGISTER_COUNT 2


class LedBar {
  private: 
    SR_74HC595 *bars[REGISTER_COUNT];
    
  public: 
    LedBar(int clock_pin, int data_pin, int latch_pin);
    void all_off();
    void show_data(byte *data);
};
