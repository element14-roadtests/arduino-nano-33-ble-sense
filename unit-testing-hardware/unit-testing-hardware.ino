/* ************************************************************************************************
* Testing implementation for all seperate parts of the hardware for the main project. 
* The three components to test: 
*   - Switch bank. 
*   - Led bar (2 74HC595's driving led bars.) 
*   - Capacitor bank. 
*   
* The components themselves will be implemented in separate classes and modules and then included in here. 
* 
************************************************************************************************ */

/* ************************************************
* Includes:
************************************************ */
#include "capacitor_bank_state_display.h"
#include "switch_bank.h"


/* ************************************************
* Enums and class declarations:
************************************************ */

/**
* General definition of states for the main StateMachine. 
*/
enum MachineState 
{
  StateIdle,
  StateTestCapBankDisplay,
  StateTestSwitchBank,
};


/**
* Class to maintain all relevant states of the application. Current state, behaviour manipulation and functions to update / read values. 
*/
class ApplicationState
{
  public:
    ApplicationState();
    
    void          advance_machine_state();
    void          mark_measurement_time();
    bool          evaluate_interval_passed(unsigned long interval);
    MachineState  get_current_machinestate();
    void          update_next_state(CapacitorBankStateDisplay *pBank_state_display);
    
  private:
    unsigned long last_measurement_msec;    
    
    MachineState    curr_machine_state;
    CapacitorState  states[4];
    int             last_state_updated;
};


/* ************************************************
* Class implementations:
************************************************ */

/**
* ApplicationState constructor. Upon instantiation, sets initial machine state and last_measurement_sec. 
*/
ApplicationState::ApplicationState() 
{
  this->curr_machine_state    = StateIdle;
  this->last_measurement_msec = 0;
  this->states[0]             = CapacitorStateLow;
  this->states[1]             = CapacitorStateLow;
  this->states[2]             = CapacitorStateLow;
  this->states[3]             = CapacitorStateLow;
  this->last_state_updated    = 0;
}


/**
* advance_machine_state: Updates machinestate, depending on current machinestate. In this case it's all pretty linear. 
*/
void ApplicationState::advance_machine_state()
{
  switch(this->curr_machine_state)
  {
    case StateIdle:               this->curr_machine_state = StateTestSwitchBank; break; 
    case StateTestCapBankDisplay: this->curr_machine_state = StateIdle;           break; 
    case StateTestSwitchBank:     this->curr_machine_state = StateIdle;           break; 
  }
}


/**
* mark_measurement_time: Set last measurement time of application instance to current time in milliseconds. The interval will then use this
*   to determine the next measurement. 
*/
void ApplicationState::mark_measurement_time()
{
  this->last_measurement_msec = millis();
}


/**
* evaluate_interval_passed: Returns true if more than 'interval' milliseconds have past since last time a measurement was initiated. 
* 
*   Input: 
*     unsigned long interval: Interval in milliseconds to compare to current time and last measurement. 
*/
bool ApplicationState::evaluate_interval_passed(unsigned long interval)
{
  unsigned long curr_time = millis();

  // If current time > interval (because of next calculation) and curr_time - interval still supersedes last measurement time, 
  //   it's time again.
  return curr_time > interval &&
    (curr_time - interval) >= this->last_measurement_msec;
}


/**
* get_current_machinestate: Return current machinestate from private field. 
*/
MachineState ApplicationState::get_current_machinestate()
{
  return this->curr_machine_state;
}


void ApplicationState::update_next_state(CapacitorBankStateDisplay *pBank_state_display)
{
  this->states[this->last_state_updated] = (CapacitorState)(this->states[this->last_state_updated] + 1 > CapacitorStateHigh ? CapacitorStateLow : this->states[this->last_state_updated] + 1);
  pBank_state_display->update_state(this->last_state_updated, this->states[this->last_state_updated]);
  this->last_state_updated = this->last_state_updated + 1 >= CAPACITOR_BANK_COUNT ? 0 : this->last_state_updated + 1;
}

  
/* ************************************************
* Global variables:
************************************************ */
ApplicationState app_state;

// Pins for the switch bank: 
int sw_bank_ch1_pin = D6;
int sw_bank_ch2_pin = D7;
int sw_bank_ch3_pin = D8;
int sw_bank_ch4_pin = D9;
int channel_pins[]  = {sw_bank_ch1_pin, sw_bank_ch2_pin, sw_bank_ch3_pin, sw_bank_ch4_pin};


// Pins for the state output: 
int state_display_data_pin  = D10;
int state_display_clock_pin = D11;
int state_display_latch_pin = D12;

// Components
CapacitorBankStateDisplay bank_state_display(state_display_clock_pin, state_display_data_pin, state_display_latch_pin, 4);
SwitchBank                switch_bank(channel_pins);


/**
* Setup core of Arduino. 
*/
void setup() 
{
  Serial.begin(9600);
  while (!Serial);

}


/**
* Write to terminal a label, value and unit to display sensor readings. 
*/
void print_data(char *label, char *unit, float value)
{
  Serial.print(label);
  Serial.print(" = ");
  Serial.print(value);
  Serial.println(unit);  
}


/**
* statemachine: Main statemachine that runs the application. No state is blocking. Rather, the idle function (probably running most of the 
*   time) should evaluate the intervaltime for measurement or other tasks. 
*/
void statemachine()
{
  switch_bank.SwitchBank_StateMachine();
  int states[SWITCH_BANK_CHANNEL_COUNT];
  
  switch( app_state.get_current_machinestate())
  {  
    case StateIdle: 
      // Wait for next interval. Every 2 seconds we execute a new measurement. 
      if(app_state.evaluate_interval_passed(500UL))
      {
        app_state.advance_machine_state();
      }
      
      break; // case StateIdle
      
    case StateTestCapBankDisplay: 
      // Update the display bank. Then go idle again. 
      Serial.println("Updating output....");
      app_state.update_next_state(&bank_state_display);
      app_state.advance_machine_state();
      app_state.mark_measurement_time();
      break; 
      
    case StateTestSwitchBank: 
      // Write data about switches to terminal. 
      switch_bank.get_bank_states(states);
      for( int i = 0; i < SWITCH_BANK_CHANNEL_COUNT; i++)
      {
        Serial.print("Channel: ");
        Serial.print(i);
        Serial.print(": state: ");
        Serial.println(states[i]);
      }
      
      app_state.advance_machine_state();
      app_state.mark_measurement_time();

  }
}


/***
* loop: Executed indefinitely, just execute the statemachine. 
*/
void loop()
{
  statemachine();  
}
