/* *****************************************************
* Basic implementation to write a byte to a 74HC595. 
* 
* Daisychaining multiple registers, one can write more bytes. 
*   this module though, assumes control of a single module. It 
*   should be regarded as such and multiple instances should be 
*   maintained. A parent control module should manage writing 
*   bytes. 
* 
***************************************************** */
#include "SR_74HC595.h"


/**
* SR_74HC595: Constructor. Set pins in local instance and configure these pins as an output pin. 
* 
*   Input: 
*     int clock_pin: Clock pin of the 595. 
*     int data_pin: Data pin of the 595. 
*     int latch_pin: Latch pin of the 595. 
*/
SR_74HC595::SR_74HC595(int clock_pin, int data_pin, int latch_pin)
{
  // Set pins for this instance. 
  this->clock_pin = clock_pin;
  this->data_pin  = data_pin;
  this->latch_pin = latch_pin;

  // Configure IO for this instance: 
  pinMode(this->clock_pin, OUTPUT);
  pinMode(this->data_pin, OUTPUT);
  pinMode(this->latch_pin, OUTPUT);
}


/**
* toggle_pin: Switch a pin high and low with a 1 ms delay. 
*/
void SR_74HC595::toggle_pin(int pin)
{
  digitalWrite(pin, HIGH);
  delay(20);
  digitalWrite(pin, LOW);
}


/**
* toggle_clock: Trigger a clock step. 
*/
void SR_74HC595::toggle_clock()
{
  // Serial.println("Clock");
  this->toggle_pin(this->clock_pin);
}


/**
* toggle_latch: Trigger a latch step to show the next data. 
*/
void SR_74HC595::toggle_latch()
{
  // Serial.println("Latch");
  this->toggle_pin(this->latch_pin);
}


/**
* display_data: Public function to write a byte to this module. 
*/
void SR_74HC595::display_data(byte data_byte, bool execute_latch)
{
  for(int i = 0; i < 8; i++)
  {
    int curr_bit = data_byte & 0x01;
    
    // Serial.print("writing bit: ");
    // Serial.print(i);
    // Serial.print(" : ");
    // Serial.println(curr_bit);
    
    digitalWrite(this->data_pin, curr_bit);
    data_byte = data_byte >> 1;
    this->toggle_clock();
  }

  if(execute_latch)
  {
    this->toggle_latch();  
  }
}


/**
* all_off: Effectively write a 0 to the chip. 
*/
void SR_74HC595::all_off()
{
  this->display_data(0, true);
}
