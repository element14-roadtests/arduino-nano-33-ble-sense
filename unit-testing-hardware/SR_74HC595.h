#include <Arduino.h>


class SR_74HC595 {
  private: 
    int clock_pin;
    int data_pin;
    int latch_pin;
  
  public: 
    SR_74HC595(int clock_pin, int data_pin, int latch_pin);  
    void display_data(byte data_byte, bool execute_latch);
    void toggle_pin(int pin);
    void toggle_clock();
    void toggle_latch();
    void all_off();
};
