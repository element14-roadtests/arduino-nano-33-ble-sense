#include <Arduino.h>
#include "led_bar.h"
#define CAPACITOR_BANK_COUNT 4


enum CapacitorState {
	CapacitorStateLow,
	CapacitorStateOk,
	CapacitorStateHigh
};


class CapacitorBankStateDisplay {
	private:
		LedBar *pLed_bar;
		CapacitorState capacitor_states[4];
		int capacitor_bank_count;
	public: 
		CapacitorBankStateDisplay(int clock_pin, int data_pin, int latch_pin, int capacitor_bank_count);
		void update_state(int channel, CapacitorState state);
    void display_new_state();
};
