#ifndef SWITCH_BANK_H
#define SWITCH_BANK_H

#include <Arduino.h>


#define SWITCH_BANK_CHANNEL_COUNT 4


enum SwitchBankStateMachine {
    SwitchBankIdle,
    SwitchBankEval,    
};

class SwitchbankChannel {
    private: 
        int current_state;
        int input_pin;
        
    public: 
        SwitchbankChannel(int inputPin);
        int get_current_state();
        void measure();
};


class SwitchBank {
    private: 
       SwitchbankChannel *channels[SWITCH_BANK_CHANNEL_COUNT];
       unsigned long last_evaluation_interval;
       SwitchBankStateMachine curr_state;
       
    public: 
        SwitchBank(int *input_pins);
        void SwitchBank_StateMachine();
        void get_bank_states(int *states);
};

#endif
