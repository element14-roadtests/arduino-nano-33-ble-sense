#include "switch_bank.h"

#define SWITCH_BANK_EVAL_INTERVAL_MS 100


SwitchbankChannel::SwitchbankChannel(int inputPin)
{
    this->input_pin     = inputPin;
    this->current_state = LOW;
}


int SwitchbankChannel::get_current_state()
{
    return this->current_state;
}


void SwitchbankChannel::measure()
{
    this->current_state = digitalRead(this->input_pin);
}



SwitchBank::SwitchBank(int *input_pins)
{
    for(int i = 0 ; i < 4; i++)
    {
        this->channels[i] = new SwitchbankChannel(input_pins[i]);
    }

    this->last_evaluation_interval = 0; 
}


void SwitchBank::SwitchBank_StateMachine()
{
  switch(this->curr_state)
  {
    case SwitchBankIdle:
      // If interval is up, evaluate state. 
      if(millis() - SWITCH_BANK_EVAL_INTERVAL_MS > this->last_evaluation_interval)
      {
        this->curr_state = SwitchBankEval;
      }
      
      break; // case SwitchBankIdle
      
    case SwitchBankEval:  
      // measure all switches and store result. Then jump back to idle. 
      for(int i = 0; i < SWITCH_BANK_CHANNEL_COUNT; i++)
      {
        this->channels[i]->measure();
      }
      
      this->curr_state = SwitchBankIdle;
      
      break; // case SwitchBankEval
  }
}


void SwitchBank::get_bank_states(int states[])
{  
  for (int i = 0; i < SWITCH_BANK_CHANNEL_COUNT; i++)
  {
    states[i] = this->channels[i]->get_current_state();
  }
}
